Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: CGI-Application
Upstream-Contact: Mark Stosberg <mark@summersault.com>
Source: https://metacpan.org/release/CGI-Application

Files: *
Copyright: 2000-2003, Jesse Erlbaum <jesse@erlbaum.net>
License: Artistic or GPL-1+

Files: r/*
Copyright: 2005, Cees Hek <ceeshek@gmail.com>
License: Artistic or GPL-1+

Files: ep/*
Copyright: 2004, Mark Stosberg <mark@summersault.com>
License: Artistic or GPL-1+

Files: sc/*
Copyright: 2008, Mark Stosberg <mark@summersault.com>
License: Artistic or GPL-1+

Files: debian/*
Copyright:
 2001-2004, Shell Hung <shell@debian.org>
 2004-2006, 2008, Jaldhar H. Vyas <jaldhar@debian.org>
 2006, Niko Tyni <ntyni@iki.fi>
 2008, Damyan Ivanov <dmn@debian.org>
 2008, Rene Mayorga <rmayorga@debian.org>
 2009, Ansgar Burchardt <ansgar@debian.org>
 2011, Dominic Hargreaves <dom@earth.li>
 2011, Nicholas Bamber <nicholas@periapt.co.uk>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
