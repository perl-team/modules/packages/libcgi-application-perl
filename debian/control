Source: libcgi-application-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Niko Tyni <ntyni@iki.fi>,
           Damyan Ivanov <dmn@debian.org>,
           Ansgar Burchardt <ansgar@debian.org>,
           Dominic Hargreaves <dom@earth.li>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: libcgi-pm-perl (>= 4.21) <!nocheck>,
                     libcgi-psgi-perl,
                     libclass-isa-perl <!nocheck>,
                     libhtml-template-perl <!nocheck>,
                     libparams-validate-perl,
                     libscalar-list-utils-perl <!nocheck>,
                     libtest-pod-coverage-perl <!nocheck>,
                     libtest-pod-perl <!nocheck>,
                     libtest-requires-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     perl
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcgi-application-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcgi-application-perl.git
Homepage: https://metacpan.org/release/CGI-Application
Rules-Requires-Root: no

Package: libcgi-application-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libcgi-pm-perl (>= 4.21),
         libclass-isa-perl,
         libmodule-build-perl,
         libparams-validate-perl,
         libscalar-list-utils-perl,
         libtest-requires-perl,
         libtest-simple-perl
Suggests: libcgi-psgi-perl,
          libhtml-template-perl
Breaks: libcgi-application-plugins-perl (<< 0.11)
Provides: libcgi-application-plugin-errorpage-perl,
          libcgi-application-plugin-redirect-perl,
          libcgi-application-standard-config-perl
Replaces: libcgi-application-plugins-perl (<< 0.11)
Description: framework for building reusable web-applications
 CGI::Application is intended to make it easier to create sophisticated,
 reusable web-based applications. This module implements a methodology
 which, if followed, will make your web software easier to design,
 easier to document, easier to write, and easier to evolve.
 .
 This package comes bundled with three extra modules providing useful glue
 run modes:
 .
 CGI::Application::Plugin::ErrorPage - automatic handling of errors
 .
 CGI::Application::Plugin::Redirect - external header based redirects
 .
 CGI::Application::Standard::Config - defines a standard interface for config
